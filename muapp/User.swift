//
//  User.swift
//  muapp
//
//  Created by Brenda Saavedra on 13/12/16.
//  Copyright © 2016 Brenda Saavedra. All rights reserved.
//

import UIKit

class User: NSObject {
    
    private var _name: String
    private var _age: Int
    private var _avatar: String
    private var _gender: String
    
    var name:String{
        return _name
    }
    
    var age:String{
        return "\(_age) años"
    }
    
    var avatar:String{
        return _avatar
    }
    
    var gender:String{
        return _gender
    }
    
    init(test:Int){
        _gender = "H"
        switch test {
        case 0:
            _name = "Brian"
            _avatar = "guy1"
            _age    = 33
            break
        case 1:
            _name = "Jeremy"
            _avatar = "guy2"
            _age    = 32
            break
        case 2:
            _name = "Elon"
            _avatar = "guy3"
            _age    = 34
            break
        case 3:
            _name = "Allan"
            _avatar = "guy4"
            _age    = 35
            break
        default:
            _name = "Manuel"
            _avatar = "guy5"
            _age    = 33
            break
        }
    }
    
}
