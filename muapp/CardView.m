//
//  CardView.m
//  muapp
//
//  Created by Brenda Saavedra on 13/12/16.
//  Copyright © 2016 Brenda Saavedra. All rights reserved.
//

#import "CardView.h"

@interface CardView ()
@end


@implementation CardView


#pragma mark - LifeCycle


- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void)setup
{
    [super setup];
    
    UITapGestureRecognizer *tapApproveImageViewGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(rightButtonAction)];
    // * Pass the touch to the next responder
    tapApproveImageViewGesture.cancelsTouchesInView = NO;
    [self.imgApprove addGestureRecognizer:tapApproveImageViewGesture];
    
    UITapGestureRecognizer *tapRejectImageViewGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(leftButtonAction)];
    tapRejectImageViewGesture.cancelsTouchesInView = NO;
    [self.imgReject addGestureRecognizer:tapRejectImageViewGesture];
}

- (void)configureCard:(int)num
{
    User *user = [[User alloc]initWithTest:num];
    self.lblAge.text = user.age;
    self.lblName.text = user.name;
    [self.imgAvatar setImage:[UIImage imageNamed:user.avatar]];
}

@end
