//
//  MatchingVC.h
//  muapp
//
//  Created by Brenda Saavedra on 12/12/16.
//  Copyright © 2016 Brenda Saavedra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
@import Spring;


@interface MatchingVC : BaseVC
@property (nonatomic, strong) IBOutlet SpringImageView *springImage;

@end

