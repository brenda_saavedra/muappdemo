//
//  AppDelegate.h
//  muapp
//
//  Created by Brenda Saavedra on 12/12/16.
//  Copyright © 2016 Brenda Saavedra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

