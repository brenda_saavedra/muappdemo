//
//  GateVC.m
//  muapp
//
//  Created by Brenda Saavedra on 12/12/16.
//  Copyright © 2016 Brenda Saavedra. All rights reserved.
//

#import "GateVC.h"
#import "muapp-Swift.h"
#import "CardView.h"

@interface GateVC () <DraggableCardViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *cardViewPlaceholder;
@property (nonatomic, strong) NSMutableArray *cardViews;
@end

@implementation GateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.cardViews = [NSMutableArray array];
    self.cardViewPlaceholder.hidden = YES;
    [self initCardViews];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self layoutCardViews];
}

- (void)initCardViews
{
    for (int ii = 0; ii < 5; ii++) {
        CardView *cardView = [[[NSBundle mainBundle] loadNibNamed:@"CardView" owner:self options:nil] firstObject];
        cardView.frame = self.cardViewPlaceholder.frame;
        cardView.delegateOfDragging = self;
        [cardView configureCard:ii];
        [self.cardViews addObject:cardView];
    }
    
    for (CardView *cardView in self.cardViews.reverseObjectEnumerator) {
        [self.view addSubview:cardView];
    }
}

- (void)layoutCardViews
{
    for (CardView *i in self.cardViews) {
        i.frame = self.cardViewPlaceholder.frame;
    }
}


@end
