//
//  BaseVC.h
//  muapp
//
//  Created by Brenda Saavedra on 12/12/16.
//  Copyright © 2016 Brenda Saavedra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseVC : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuBtn;
@end
