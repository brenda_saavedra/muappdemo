//
//  ChatCell.swift
//  muapp
//
//  Created by Brenda Saavedra on 13/12/16.
//  Copyright © 2016 Brenda Saavedra. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {

    
    @IBOutlet weak var imgAvatar: CircularImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    var message:Message!
    
    func configureCell(){
        self.imgAvatar.image = UIImage(named: message.user.avatar)!
        self.lblName.text = message.user.name
        self.lblTime.text = message.time
        self.lblMessage.text = message.message
    }
    
}
