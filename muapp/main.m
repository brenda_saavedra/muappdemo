//
//  main.m
//  muapp
//
//  Created by Brenda Saavedra on 12/12/16.
//  Copyright © 2016 Brenda Saavedra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
