//
//  ChatVC.m
//  muapp
//
//  Created by Brenda Saavedra on 12/12/16.
//  Copyright © 2016 Brenda Saavedra. All rights reserved.
//

#import "ChatVC.h"
#import "muapp-Swift.h"


@implementation ChatVC


- (void)viewDidLoad {
    [super viewDidLoad];
    arrMessages = @[[[Message alloc]initWithTest:0],
                    [[Message alloc]initWithTest:1],
                    [[Message alloc]initWithTest:2],
                    [[Message alloc]initWithTest:3],
                    [[Message alloc]initWithTest:4]];
    
    self.tableView.tableFooterView = [UIView new];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrMessages.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"cell";
    ChatCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)  {
        cell = [[ChatCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:identifier];
    }
    
    Message *message = [arrMessages objectAtIndex:indexPath.row];
    cell.message = message;
    [cell configureCell];

    return cell;
}



@end
