//
//  MatchingVC.m
//  muapp
//
//  Created by Brenda Saavedra on 12/12/16.
//  Copyright © 2016 Brenda Saavedra. All rights reserved.
//

#import "MatchingVC.h"

@implementation MatchingVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.springImage.animation = @"pop";
    self.springImage.curve = @"easeIn";
    self.springImage.duration = 3.5;
    self.springImage.scaleX = 5.0;
    self.springImage.scaleY = 5.0;
    self.springImage.rotate = 1.9;
    self.springImage.damping = 0.3;
    self.springImage.velocity = 0.5;
    self.springImage.autostart = true;
    [self.springImage animate];
}



@end
