//
//  CardView.h
//  muapp
//
//  Created by Brenda Saavedra on 13/12/16.
//  Copyright © 2016 Brenda Saavedra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DraggableCardView.h"
#import "muapp-Swift.h"


@interface CardView : DraggableCardView <UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblContacts;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblAge;
@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UIImageView *imgReject;
@property (weak, nonatomic) IBOutlet UIImageView *imgApprove;

- (void)configureCard:(int)num;

@end

