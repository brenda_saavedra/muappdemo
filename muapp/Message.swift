//
//  Message.swift
//  muapp
//
//  Created by Brenda Saavedra on 13/12/16.
//  Copyright © 2016 Brenda Saavedra. All rights reserved.
//

import UIKit

class Message: NSObject {
    
    private var _user: User
    private var _message: String
    private var _time: String
    
    var user:User{
        return _user
    }
    
    var message:String{
        return _message
    }
    
    var time:String{
        return _time
    }
    
    
    init(test:Int){
        switch test {
        case 0:
            _user = User(test: 0)
            _message = "Hey buenos días"
            _time =  "8:00"
            break
        case 1:
            _user = User(test: 1)
            _message = "Hey buenos días"
            _time =  "8:00"
            break
        case 2:
            _user = User(test: 2)
            _message = "Hey buenos días"
            _time =  "8:00"
            break
        case 3:
            _user = User(test: 3)
            _message = "Hey buenos días"
            _time =  "8:00"
            break
        default:
            _user = User(test: 4)
            _message = "Hey buenos días"
            _time =  "8:00"
            break
        }
    }
    
    
    
}

