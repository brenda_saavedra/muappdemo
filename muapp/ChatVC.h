//
//  ChatVC.h
//  muapp
//
//  Created by Brenda Saavedra on 12/12/16.
//  Copyright © 2016 Brenda Saavedra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

@interface ChatVC : BaseVC <UITableViewDelegate, UITableViewDataSource>{
    NSArray *arrMessages;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
